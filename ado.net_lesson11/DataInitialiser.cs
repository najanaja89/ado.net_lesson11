﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson11
{
    public class DataInitialiser: DropCreateDatabaseAlways<CitiesContext>
    {
        protected override void Seed(CitiesContext context)
        {
            context.Cities.AddRange(new List<City>
            {
               new City { Name= "Astana", Population = 1200000},
               new City { Name= "Almaty", Population = 2000000},
               new City { Name= "Satpayev", Population = 30000},
            });
            context.SaveChanges();
        }
    }
}
