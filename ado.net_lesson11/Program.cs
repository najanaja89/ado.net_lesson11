﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson11
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new CitiesContext())
            {
                foreach (var city in context.Cities.ToList())
                {
                    Console.WriteLine($"City name: {city.Name}\tPopulation: {city.Population}");
                }
                Console.ReadLine();
            }
        }
    }
}
